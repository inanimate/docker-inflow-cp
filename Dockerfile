#FROM debian:wheezy

FROM node:6.3
ENV NODE_DIR /opt/node
RUN mkdir ${NODE_DIR}
WORKDIR ${NODE_DIR}
COPY package.json ./package.json
RUN npm install
RUN echo "export PATH=$PATH:${NODE_DIR}/node_modules/.bin" >> /root/.bashrc

